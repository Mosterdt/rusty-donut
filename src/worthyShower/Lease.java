package worthyShower;

/**
 * The Class Lease.
 */
public class Lease {

	/** The active. */
	private boolean active;
	
	/** The duration. */
	private long duration;
	
	/** The ip. */
	private int ip;
	
	/** The mac. */
	private long mac;
	
	/** The time begin. */
	private long timeBegin;
	
	/** The time end. */
	private long timeEnd;

	/**
	 * Instantiates a new lease.
	 *
	 * @param mac the mac
	 * @param ip the ip
	 * @param timeBegin the time begin
	 * @param duration the duration
	 */
	Lease(long mac, int ip, long timeBegin, long duration) {
		this.mac = mac;
		this.ip = ip;
		this.timeBegin = timeBegin;
		this.duration = duration;
		this.timeEnd = timeBegin + duration;
	}

	/**
	 * Gets the duration.
	 *
	 * @return the duration
	 */
	public long getDuration() {
		return duration;
	}

	/**
	 * Gets the ip.
	 *
	 * @return the ip
	 */
	public int getIp() {
		return ip;
	}

	/**
	 * Gets the lease time left.
	 *
	 * @return the lease time left
	 */
	public long getLeaseTimeLeft() {
		return this.timeEnd - System.currentTimeMillis();
	}

	/**
	 * Gets the mac.
	 *
	 * @return the mac
	 */
	public long getMac() {
		return this.mac;
	}

	/**
	 * Gets the time begin.
	 *
	 * @return the time begin
	 */
	public long getTimeBegin() {
		return timeBegin;
	}

	/**
	 * Gets the time end.
	 *
	 * @return the time end
	 */
	public long getTimeEnd() {
		return timeEnd;
	}

	/**
	 * Checks if is active.
	 *
	 * @return true, if is active
	 */
	public boolean isActive() {
		return this.active;
	}

	/**
	 * Renew.
	 */
	public void renew() {
		this.setTimeBegin(System.currentTimeMillis());
		this.setTimeEnd(this.getTimeBegin() + this.getDuration());
		this.setActive(true);
	}

	/**
	 * Sets the active.
	 *
	 * @param active the new active
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * Sets the duration.
	 *
	 * @param duration the new duration
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}

	/**
	 * Sets the ip.
	 *
	 * @param ip the new ip
	 */
	public void setIp(int ip) {
		this.ip = ip;
	}

	/**
	 * Sets the mac.
	 *
	 * @param mac the new mac
	 */
	public void setMac(long mac) {
		this.mac = mac;
	}

	/**
	 * Sets the time begin.
	 *
	 * @param timeBegin the new time begin
	 */
	public void setTimeBegin(long timeBegin) {
		this.timeBegin = timeBegin;
	}

	/**
	 * Sets the time end.
	 *
	 * @param timeEnd the new time end
	 */
	public void setTimeEnd(long timeEnd) {
		this.timeEnd = timeEnd;
	}

	/* 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(DHCPPacket.longToMac(this.getMac()).substring(0, 17));
		sb.append(" | ");
		sb.append(DHCPPacket.intToIPString(this.getIp()));
		sb.append(" | ");
		sb.append(this.getLeaseTimeLeft() / 1000).append("s");

		return sb.toString();
	}

	/**
	 * Update.
	 */
	public void update() {
		this.active = getLeaseTimeLeft() > 0;
	}
}
